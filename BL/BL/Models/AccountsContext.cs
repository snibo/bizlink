﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BL.Models
{
    public class AccountsContext : DbContext
    {
        public AccountsContext (DbContextOptions<AccountsContext> options):
            base(options)
        {

        }
        public DbSet<Account> accounts { get; set; }

    }
}
