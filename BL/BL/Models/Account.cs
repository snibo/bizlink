﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Models
{
    public class Account
    {
        public long Id { get; set; }
        public string UserPassword { get; set; }
        public string UserFirstName{ get; set; }
        public char UserMiddleInitial { get; set; }
        public string UserLastName { get; set; }
        public byte Sex { get; set; }
        public string UserEmail { get; set; }
        public string BusinessName { get; set; }
        public DateTime DateCreated { get; set; }
        //public Account CreatedBy{ get; set; }
        public DateTime DateModified { get; set; }
        //public Account ModifiedBy { get; set; }

        //public Role UserRole{ get; set; }
        //public byte Status{ get; set; }
        //public long RewardID{ get; set; }

    }
}
